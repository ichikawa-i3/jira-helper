// localStorageのキー
var key = "setting";  
// localStorageの文字列をJSONで取得
var getObject = function() {
    var str = localStorage.getItem(key);
    return JSON.parse(str);
};

// JSONを文字列でlocalStorageに保存
var setObject = function(obj) {
    var str = JSON.stringify(obj);
    localStorage.setItem(key, str);
};

// localStorageに保存したデータの表示
var showStorage = function() {
    var obj = getObject();
    $('#domain').val(obj['domain']);
	if(obj['opentype'] == 1) $('#opentype').prop('checked', 'checked');	    
};

$(function(){
    // オプションデータの更新
    $('#put').click(function() {
        var domain_value = $('#domain').val();
        var opentype_value = ($('#opentype').prop('checked')) ? 1 : 0;
        var obj = getObject();
        if (!obj) {
            obj = new Object();
        }
        obj["domain"] = domain_value;
        obj["opentype"] = opentype_value;
        setObject(obj);
        showStorage();
        alert("Update OK");
    });
    // オプションデータの表示
    showStorage();
});
